const mongoose = require('mongoose');
const blockModel = require('./blockModel');
const url = 'mongodb://localhost:27017/mvc';

async function insertBlock(data) {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        let block = new blockModel(data);

        await block.save();

        mongoose.connection.close();
    } catch (err) {
        console.log(err);
    }
}

async function findBlockById(id) {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        let block = await blockModel.findById(id);
        mongoose.connection.close();
        return block;
    } catch (err) {
        console.log(err);
        return null;
    }
}

async function findLastBlock() {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        let block = await blockModel.findOne({}, {}, {
            sort: {
                _id: -1
            }
        });

        mongoose.connection.close();
        return block;
    } catch (err) {
        console.log(err);
        return null;
    }
}
module.exports = {
    insertBlock,
    findBlockById,
    findLastBlock
}