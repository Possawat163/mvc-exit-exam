const blockCollection = require('../blockCollection');

//5
async function addBlockToChain(message) {
    let regex = /\d*/;
    if (message == null) {
        return {
            _id: null,
            currentHash: null,
            previousHash: null,
            data: null,
            response: 'accept integer'
        };
    }
    if (!regex.test(message)) {
        console.log('error');
        return {
            _id: null,
            currentHash: null,
            previousHash: null,
            data: null,
            response: 'accept integer'
        };
    }

    let data = parseInt(message, 10);
    let preBlock = await blockCollection.findLastBlock();
    //2
    if (preBlock == null) {
        await startBlock();
        preBlock = await blockCollection.findLastBlock();
    }
    //4
    let blockId = preBlock._id + 1;

    let currentHash = await hashCalculate(blockId, preBlock.currentHash, data);

    let previousHash = preBlock.currentHash;

    let block = {
        _id: blockId,
        currentHash: currentHash,
        previousHash: previousHash,
        data: data
    }
    console.log('block', block);
    //1
    if (block._id == null ||
        block.currentHash == null ||
        block.previousHash == null ||
        block.data == null) {
        return {
            _id: blockId,
            currentHash: currentHash,
            previousHash: previousHash,
            data: data,
            response: 'information is null cannot insert'
        };
    }

    await blockCollection.insertBlock(block);
    return {
        _id: blockId,
        currentHash: currentHash,
        previousHash: previousHash,
        data: data,
        response: ' insert success'
    };
}

async function readBlockChain(id) {
    let block = await blockCollection.findBlockById(id);
    console.log(block);
    if (block == null) {
        return {
            _id: null,
            currentHash: null,
            previousHash: null,
            data: null,
            response: 'no block id'
        };
    }
    return {
        _id: block._id,
        currentHash: block.currentHash,
        previousHash: block.previousHash,
        data: block.data,
        response: ' read success'
    };
}

//3
async function hashCalculate(id, previousBlockHash, data) {
    let sum = id + previousBlockHash + data;
    console.log(sum);

    while (sum.toString().length != 1) {
        let sumString = sum.toString();
        let length = sumString.length;
        let tempSum = 0;
        for (let i = 0; i < length; i++) {
            tempSum += parseInt(sumString[i], 10);
            // console.log(i, tempSum);
        }
        sum = tempSum;
    }
    console.log(sum);
    return sum;
}

async function startBlock() {
    try {
        let block = {
            _id: 0,
            currentHash: 1,
            previousHash: 0,
            data: 100
        };
        await blockCollection.insertBlock(block);
        return 'ok';
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    addBlockToChain,
    readBlockChain
}