const mongoose = require('mongoose');
const schema = mongoose.Schema;

const blockSchema = new schema({
    _id: Number,
    currentHash: Number,
    previousHash: Number,
    data: Number
});

const blockModel = mongoose.model('block', blockSchema);

module.exports = blockModel;