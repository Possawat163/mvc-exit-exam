const express = require('express');
const router = express.Router();
const blockCollection = require('../model/blockCollection');
const blockLogic = require('../model/logic/blockLogic');

/* GET home page. */
router.get('/', async (req, res) => {
  let response = {
    _id: null,
    currentHash: null,
    previousHash: null,
    data: null,
    response: null
  };
  res.render('index', response);
});

router.post('/addblock', async (req, res) => {
  console.log('router', req.body.value);
  let response = await blockLogic.addBlockToChain(req.body.value);
  res.render('index', response);
});

router.post('/getblock', async (req, res) => {
  // console.log('router', req.body._id);
  let result = await blockLogic.readBlockChain(req.body._id);
  res.render('index', result);
});


module.exports = router;